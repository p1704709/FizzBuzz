/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author p1704709
 */
public class FizzBuzzTest {
    
    public FizzBuzzTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void nonDivisible() {
        FizzBuzz instance = new FizzBuzz();
        assertEquals("1", instance.traitement(1));
        assertEquals("2", instance.traitement(2));
        assertEquals("4", instance.traitement(4));
    }
    
    @Test
    public void divisible_3() {
        FizzBuzz instance = new FizzBuzz();
        assertEquals("Fizz", instance.traitement(3));
        assertEquals("Fizz", instance.traitement(-3));
        assertEquals("Fizz", instance.traitement(6));
    }
    
    @Test
    public void divisible_5() {
        FizzBuzz instance = new FizzBuzz();
        assertEquals("Buzz", instance.traitement(5));
        assertEquals("Buzz", instance.traitement(-5));
        assertEquals("Buzz", instance.traitement(10));
    }
    
    @Test
    public void divisible_3_5() {
        FizzBuzz instance = new FizzBuzz();
        assertEquals("FizzBuzz", instance.traitement(15));
        assertEquals("FizzBuzz", instance.traitement(-15));
        assertEquals("FizzBuzz", instance.traitement(30));
    }
    
    
    
}
